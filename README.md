# Flower Pot Watering

Startup Emulator
Flower Pot Watering

An open-source project for development tools for watering flowers.

## Install packages.

```bash
npm install
```

## Run app 

```bash
npm run ionic serve
```

## Run lint 

```bash
npm run lint
```

## API

### Installation

```bash
$ cd api
$ npm install
```

### MongoDB
* Install and run MongoDB locally following the <a target="_blank" href="https://docs.mongodb.com/manual/administration/install-community/">manual</a>.
* Inside of ```api``` directory create ```.env``` file:
  ```
    MONGO_URI=mongodb://localhost:27017/fpw
  ```

Draft of database scheme is located in `api/docs/db`.

Scheme has been created with <a target="_blank" href="https://app.diagrams.net/">app.diagrams.net</a>

### Running the API

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

### Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
