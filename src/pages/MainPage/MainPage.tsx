import React from 'react';
import { IonContent, IonPage } from '@ionic/react';
import { colorFillOutline } from 'ionicons/icons';
import { connect } from 'react-redux';
import Header from '../../modules/Header/components/Header';
import FlowerPotSlider from '../../modules/FlowerPotsSlider/FlowerPotsSlider';
import FlowerPotsKeyFeatures from '../../modules/FlowerPotsKeyFeatures/FlowerPotsKeyFeatures';
import ButtonWithIcon from '../../common/ButtonWithIcon/ButtonWithIcon';
import { AppStateType } from '../../reducers/rootReducer';
import './MainPage.scss';

type MainPageType = {
  isLoggedIn: undefined | boolean;
};

function MainPage({ isLoggedIn }: MainPageType): JSX.Element {
  return (
    <IonPage>
      <Header />
      <IonContent className="landing-page-content">
        <h1 className="landing-page-content__header ion-margin-top ion-margin-bottom">
          Track your flowers within the application
          <span className="landing-page-content__header2">startup emulator 2020</span>
        </h1>
        <FlowerPotSlider />
        {isLoggedIn && (
          <ButtonWithIcon
            link="/shelves"
            color="white"
            text="Go To Shelves"
            iconName={colorFillOutline}
            className="landing-page-content__button ion-padding-end" />
        )}
        <FlowerPotsKeyFeatures />
      </IonContent>
    </IonPage>
  );
}

const mapStateToProps = (state: AppStateType) => ({
  isLoggedIn: state.login.isLoggedIn,
});

export default connect(mapStateToProps)(MainPage);
