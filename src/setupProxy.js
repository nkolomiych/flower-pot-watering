const { createProxyMiddleware } = require('http-proxy-middleware');

function proxy(app) {
  app.use(
    '/api',
    createProxyMiddleware({
      target: 'http://localhost:3005',
      changeOrigin: true,
    }),
  );
}

module.exports = proxy;
